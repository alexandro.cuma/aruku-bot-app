import { actionNames } from '../actions';

interface Info {
    broadcaster_type: string
    description: string
    username: string
    picture: string
    email: string
}

interface InfoState {
    loading: boolean,
    info: Info
}

const initialState: InfoState = {
    loading: false,
    info: undefined
}

export const infoReducer = (state: InfoState = initialState, action:any) => {
    switch(action.type) {
        case actionNames.USER_FETCH_INFO_ATTEMPT: {
            return {
                ...state,
                loading: true
            }
        }
        case actionNames.USER_FETCH_INFO_COMPLETED: {
            return {
                ...state,
                loading: false
            }
        }
        case actionNames.USER_FETCH_INFO_SUCCESS: {
            if(action.payload.info){
                return {
                    ...state,
                    loading: false,
                    info: action.payload.info
                }
            }
            return state
        }
        default:
            return state
    }
}