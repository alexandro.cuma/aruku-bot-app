import axios from 'axios';

const url = 'http://192.168.1.68:4000/api'

export const getCommands = async () => {
    let resp = await axios.get(url+'/commands')
    return resp.data.data
}

export const createCommands = async (command:any) => {
    let resp = await axios.post(url+'/commands',{ command: command })
    return resp.data.data
}

export const updateCommands = async () => {
    let resp = await axios.get(url+'/commands')
    return resp.data.data
}

export const deleteCommands = async () => {
    let resp = await axios.get(url+'/commands')
    return resp.data.data
}
