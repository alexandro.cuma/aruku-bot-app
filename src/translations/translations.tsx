export type Translation = {
    /*
        Common Actions
    */
    BACK: string
    /*
        Validations  
    */
    EMAIL_VALIDATION: string,
    /*
        Login
    */
    LOGIN: string
    LOGOUT: string
    LOGIN_INPUT_USERNAME: string
    LOGIN_INPUT_PASSWORD: string    
    /*
        Registration
    */
    SIGNUP: string
    REGISTRATION: string
    SIGNUP_INPUT_NAME: string
    SIGNUP_INPUT_LASTNAME: string
    SIGNUP_INPUT_REPASSWORD: string
    SIGNUP_CHECK_TERMSCOND: string
    SIGNUP_ERROR_MESSAGE: string
}