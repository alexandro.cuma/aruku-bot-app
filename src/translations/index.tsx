import i18n from 'i18next'
import * as Languages from './languages';
import { initReactI18next } from 'react-i18next';

const DEFAULT_LANGUAGE = 'es_MX'

const resources = {
    es_MX: {
        translation: {
            ...Languages.esMX,
        },
    },
    en_US: {
        translation: {
            ...Languages.enUS,
        }
    }
}

i18n
.use(initReactI18next)
.init({
    lng: DEFAULT_LANGUAGE,
    resources: resources
})

export default i18n