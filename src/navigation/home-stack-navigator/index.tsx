import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { StreamDrawNavigation } from '../stream-drawer-navigator';

import { Home, Commands, Variables, Timers, MyAccount } from '../../screens';

const Stack = createStackNavigator();

export const HomeStackNavigation = () => {
  return (
      <Stack.Navigator
        initialRouteName="Home"
        >
        <Stack.Screen options={{headerShown: false}} name="Stream" component={StreamDrawNavigation} />
        <Stack.Screen options={{headerShown: false}} name="Variables" component={Variables} />
        <Stack.Screen options={{headerShown: false}} name="MyAccount" component={MyAccount} />
        <Stack.Screen options={{headerShown: false}} name="Commands" component={Commands} />
        <Stack.Screen options={{headerShown: false}} name="Timers" component={Timers} />
        <Stack.Screen options={{headerShown: false}} name="Home" component={Home} />
      </Stack.Navigator>
  );
}
