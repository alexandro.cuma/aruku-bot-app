import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { getCommands } from '../../services';
import { View, StyleSheet } from 'react-native';
import { ListItem, Text } from 'react-native-elements';

const commandList:any = []

export const CommandList = () => {
    const command = useSelector(state => state.command.command)
    
    useEffect(()=>{
        getCommands()
        .then(data => {
            console.log(data)
        })
        .catch(() => {

        })
        if(command){
            let index = commandList.findIndex(x => x.command === command.command)
            if(index < 0){
                commandList.push(command)
            }
        }
    })
    return(
        <View style={styles.container}>
            <Text style={styles.textStyle} h3>Comandos de Directo</Text>
            {
                commandList.map((l, i) => (
                <ListItem containerStyle={styles.listItem} key={i} bottomDivider>
                    {/* <Avatar source={{uri: l.avatar_url}} /> */}
                    <ListItem.Content>
                    <ListItem.Title style={styles.listTextStyle}>
                        {l.command}
                    </ListItem.Title>
                    <ListItem.Subtitle style={styles.listSubTextStyle}>
                        {l.cooldown}
                    </ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
                ))
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0e0e10'
    },
    textStyle: {
        textAlign: 'center',
        marginBottom: 20,
        marginTop: 20,
        color:'lightgray'
    },
    listItem: {
        backgroundColor: '#18181b',
    },
    listTextStyle: {
        fontSize: 20,
        color: 'lightgray'
    },
    listSubTextStyle: {
        fontSize: 16,
        color: 'lightgray'
    }
})