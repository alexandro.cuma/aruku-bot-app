import * as React from 'react';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';

import { LogStackNavigation } from './log-stack-navigator';
import { HomeStackNavigation } from './home-stack-navigator';

export const AppContainer = () =>{
  const user = useSelector(state => state.login.user)
  console.log(user)
  return (
    <NavigationContainer>
      {
        user? (
          <HomeStackNavigation />
        ):(
          <LogStackNavigation />
        )
      }
    </NavigationContainer>
  );
}
