import React from 'react';
import { Reducers } from './store';
import { Provider } from 'react-redux';
import { AppContainer } from './navigation';
import { PersistGate } from 'redux-persist/integration/react';
import { globalStore, persistorStore, createGlobalStore } from './store'; 

declare const global: {HermesInternal: null | {}};


const App = () => {
  createGlobalStore(Reducers)
  return (
      <Provider store={globalStore()}>
        <PersistGate loading={null} persistor={persistorStore()}>
          <AppContainer/>
        </PersistGate>
      </Provider>
  );
};

export default App;
