import { actionNames } from '../actions';

interface User {
    access_token: string
   refresh_token: string
}

interface loginState {
    loading: boolean,
    user: User
}

const initialState: loginState = {
    loading: false,
    user: undefined
}

export const loginReducer = (state: loginState = initialState, action) => {
    switch(action.type) {
        case actionNames.ARUKUBOT_LOGIN_ATTEMPT: {
            return {
                ...state,
                loading: true
            }
        }
        case actionNames.ARUKUBOT_LOGIN_COMPLETED: {
            return {
                ...state,
                loading: false
            }
        }
        case actionNames.ARUKUBOT_LOGIN_SUCCESS: {
            if(action.payload.user){
                return {
                    ...state,
                    loading: false,
                    user: action.payload.user
                }
            }
            return 
        }
        case actionNames.ARUKUBOT_USER_LOGOUT: {
            return {
                state: undefined
            }
        }
        default:
            return state
    }
}