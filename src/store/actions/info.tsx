import { Action, AnyAction } from 'redux';
import { actionNames } from './index';

export const onInfoAttempt: Action = {
    type: actionNames.USER_FETCH_INFO_ATTEMPT,
}

export const onInfoCompleted: Action = {
    type: actionNames.USER_FETCH_INFO_COMPLETED,
}

export const onInfoSuccess = (info: any): AnyAction => ({
    payload: { info },
    type: actionNames.USER_FETCH_INFO_SUCCESS,
})
