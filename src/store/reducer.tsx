import { combineReducers } from 'redux';
import { infoReducer } from './reducers/info';
import { loginReducer } from './reducers/login';
import { commandReducer } from './reducers/commands';
import { variableReducer } from './reducers/variables';

const allReducers = combineReducers({
    variable: variableReducer,
    command: commandReducer,
    login: loginReducer,
    info: infoReducer
})

export const Reducers = (state:any, action:any) => {
    return allReducers(state, action)
}