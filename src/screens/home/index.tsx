import { useSelector } from 'react-redux' 
import { Text } from 'react-native-elements'
import React, { useState, useEffect } from 'react'
import { getUserName, getUserInfo } from '../../services'
import { View, StyleSheet, Dimensions } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { QuickAccessButton, ProfileCard, InfoCard } from '../../components'

const { width, height } = Dimensions.get('window')
const DirectAccess = [
   { icon: "book", name: "Variables", type:'font-awesome' },
   { icon: "alert-outline", name: "Commands", type:'ionicon' },
   { icon: "timer", name: "Timers",  type:undefined }
]
export const Home = ({ navigation }:any) => {
    const [Info, setInfo] = useState(null)
    const user = useSelector(state => state.login.user)
    useEffect(()=>{
        if(user && !Info){
            getUserName(user)
            .then(data=>{    
                console.log(data)    
                getUserInfo(data[1], data[0].preferred_username)
                .then(data=>{
                    console.log(data)
                    setInfo(data)
                })
            })
            .catch(err =>{
                console.log(err)
            })
        }
    })
    const goTo = () => {
        navigation.navigate("MyAccount")
    }
    const showProfileCard = () => {
        if(Info){
            return <ProfileCard onPress={goTo} Info={Info} />
        }
    }
    const showInfoCard = () => {
        if(Info){
            return <InfoCard Info={Info} />
        }
    }
    return(
        <View style={styles.container}>
            {showProfileCard()}
            {showInfoCard()}
            <Text h4 style={{textAlign: 'center', color: 'lightgray', marginTop: 25}}> Accesos Directos </Text>
            <View style={styles.buttonContainer}>
                {
                    DirectAccess.map((value, index)=>(
                        <QuickAccessButton key={index+'_AD'} value={value} navigation={navigation} />
                    ))
                }
            </View>
            <View style={{marginTop: 30}}>
                <TouchableOpacity
                    style={styles.streamButton}
                    onPress={() => navigation.navigate('Stream', { username: Info.login })}
                    >
                    <Text style={{textAlign: 'center', color: 'white', fontSize: 16}}>
                        Entrar al Panel de Stream
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    )   
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#0e0e10',
    }, 
    buttonContainer: {
        justifyContent:'space-between',
        flexDirection: 'row',
        margin: 10
    },
    buttonStyle: {
        width: 70,
        height: 70,
        borderRadius: 50,
        backgroundColor: 'gray',
        justifyContent: 'center',
    },
    streamButton: {
        width: width*.7,
        borderRadius: 10,
        height: height*.07,
        justifyContent: 'center',
        backgroundColor: '#9146FF',
    }
})