import * as Yup from 'yup';
import i18n from '../../translations';

export const Validations = Yup.object({
    command: 
        Yup.string()
        .trim()
        .required(i18n.t('SIGNUP_ERROR_MESSAGE')),
    action: 
        Yup.string()
        .trim()
        .required(i18n.t('SIGNUP_ERROR_MESSAGE')),
})