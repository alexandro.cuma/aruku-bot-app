import { onVariableAttempt, onVariableSuccess, onVariableCompleted } from '../actions/variables';

export const attemptVariable = ({name, type, initialValue}: any) => (dispatch:any) => {
    dispatch(onVariableAttempt)
    setTimeout(()=>{
        if(name && type && initialValue){
                dispatch(onVariableCompleted)
                dispatch(onVariableSuccess({name,type,initialValue})) 
            
        }else{
            dispatch(onVariableCompleted)
        }
    },2000)
}
