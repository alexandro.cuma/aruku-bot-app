import { Translation } from '../translations';

export const esMX: Translation = {
    /*
        Common Actions
    */
    BACK: 'Regresar',
    /*
        Validations  
    */
    EMAIL_VALIDATION: 'Escribe un correo válido',
    /*
        Login
    */ 
    LOGIN: 'Iniciar Sesión',
    LOGOUT: 'Cerrar Sesión',
    LOGIN_INPUT_USERNAME: 'Escribe tu correo...',
    LOGIN_INPUT_PASSWORD: 'Escribe tu contraseña...',
     /*
        Registration
    */
   SIGNUP: 'Registrarse',
   REGISTRATION: '¿Aún no tienes cuenta? Regístrate',
   SIGNUP_INPUT_NAME: 'Escribe tu nombre...',
   SIGNUP_INPUT_LASTNAME: 'Escribe tu(s) apellido(s)...',
   SIGNUP_INPUT_REPASSWORD: 'Reescribe tu contraseña...',
   SIGNUP_CHECK_TERMSCOND: 'Aceptar términos y condiciones de servicio',
   SIGNUP_ERROR_MESSAGE: 'Campo Requerido',
}
