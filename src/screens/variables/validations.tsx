import * as Yup from 'yup';
import i18n from '../../translations';

export const Validations = Yup.object({
    name: 
        Yup.string()
        .trim()
        .required(i18n.t('SIGNUP_ERROR_MESSAGE')),
    initialNumbValue: 
        Yup.string()
        .required(i18n.t('SIGNUP_ERROR_MESSAGE')),
    initialTextValue: 
        Yup.string()
        .trim()
        .required(i18n.t('SIGNUP_ERROR_MESSAGE')),
})