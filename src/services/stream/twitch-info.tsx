import axios from 'axios';

const clientId = '7j3zgcd85tmodozj7b2xjnx193e8gy'
const UserNameURL = 'https://id.twitch.tv/oauth2/userinfo' 
const UserInfoURL = 'https://api.twitch.tv/helix/users?login='

export const getUserName = async (user:any) => {    
    const config = {
        headers: {
            'Authorization': 'Bearer ' + user.access_token,
        }
    }
    try {
        let resp = await axios.get(UserNameURL, config)
        return [resp.data, user.access_token]
    } catch (err) {
        console.log(err)
        return err
    }
}

export const getUserInfo = async (token: string, name: string) => {
    const config = {
        headers: {
            'Authorization': 'Bearer ' + token,
            'Client-ID': clientId
        }
    }
    try {
        let resp = await axios.get(UserInfoURL+name, config)
        return resp.data.data[0]
    } catch (err) {
        console.log(err)
    }
}