import React from 'react';
import { useSelector } from 'react-redux'
import { WebView } from 'react-native-webview'
import { Icon, Text } from 'react-native-elements' 
import { View, TouchableOpacity, StyleSheet } from 'react-native' 


export const TwitchChat = ({ username, navigation }:any) => {
    return(
        <View style={{ flex: 1 }}  >
            <TouchableOpacity
                onPress={() => navigation.closeDrawer()}
                style={styles.closeButtonStyle}
            >
                <Text style={{ fontSize: 16 }}>Cerrar chat</Text>
                <Icon
                    size={20}
                    name="close"
                    color="black"
                />
            </TouchableOpacity>
            <WebView
                source={{ uri: "https://www.twitch.tv/embed/"+username+"/chat?parent=streamernews.example.com" }}
                style={{ width: '100%'}}        
                scalesPageToFit    
            />
        </View>
    )
}

const styles = StyleSheet.create({
    closeButtonStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 3.84,
        elevation: 5,
        alignItems: 'center',
        alignSelf: 'flex-end',
        backgroundColor: 'white',
        justifyContent:'center',
        flexDirection:'row',
        height: 40,
        width: '100%',
    },
})