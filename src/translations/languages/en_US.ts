import { Translation } from '../translations';

export const enUS: Translation = {
     /*
        Common Actions
    */
    BACK: 'Back',
    /*
        Validations  
    */
    EMAIL_VALIDATION: 'Write a valid e-mail',
    /*
        Login
    */ 
    LOGIN: 'Login',
    LOGOUT: 'Logout',
    LOGIN_INPUT_USERNAME: 'Write your email...',
    LOGIN_INPUT_PASSWORD: 'Write your password...',
     /*
        Registration
    */
   SIGNUP: 'Signup',
   REGISTRATION: '¿Have no account? Register Here',
   SIGNUP_INPUT_NAME: 'Write your name...',
   SIGNUP_INPUT_LASTNAME: 'Write your lastname...',
   SIGNUP_INPUT_REPASSWORD: 'Rewrite your password...',
   SIGNUP_CHECK_TERMSCOND: 'Agree terms and conditions of services',
   SIGNUP_ERROR_MESSAGE: 'Field Needed',
}
