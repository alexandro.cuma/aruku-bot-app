import React from 'react'
import { View, Dimensions, StyleSheet } from 'react-native'
import { Card, Text, Divider, Icon } from 'react-native-elements'

const { width, height } = Dimensions.get('window')

export const InfoCard = ({ Info }: any) => {
    const broadType = () => {
        if(Info.broadcaster_type === "affiliate"){
            return(
                <View>
                    <Text style={styles.textStyle}>Tipo de Streamer:</Text>
                    <Text h4 style={styles.textStyle}>Afiliado</Text>
                </View>
            )
        }
    }
    const broadSign = () => {
        return (
            <View>
                <Icon 
                    size={30}
                    color="white"
                    type="ionicon"
                    name="medal-outline"
                />
            </View>
        )
    }
    return(
        <Card containerStyle={styles.container}>
            <View style={{alignItems: 'center', flexDirection: 'row', justifyContent: 'space-around'}}>
                {broadSign()}
                {broadType()}
            </View>
            <Divider />
            <View style={{ marginRight: width*.11, marginLeft: 10 }}>
                <Text style={styles.textStyle}>
                    Descripción:
                </Text>
                <Text style={[styles.textStyle,{ fontSize: 12 }]}>
                    {Info.description}
                </Text>
            </View>
            <Divider />
            <View style={{ marginRight: width*.11, marginLeft: 10 }}>
                <Text style={styles.textStyle}>
                    Vistas Totales:
                </Text>
                <Text style={[styles.textStyle,{ fontSize: 12 }]}>
                    {Info.view_count}
                </Text>
            </View>
        </Card>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#1f1f1f',
        marginTop: height*.05,
        borderColor: 'black',
        height: height*.25,
        width: width,
    },
    textStyle: {
        color: 'lightgray',
        textAlign: 'right'
    }
})