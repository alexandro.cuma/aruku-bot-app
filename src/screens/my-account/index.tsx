import React from 'react'
import { useDispatch } from 'react-redux'
import { Icon } from 'react-native-elements'
import { attemptLogout } from '../../store/thunks';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

export const MyAccount = () =>{
    const dispatch = useDispatch()
    const Logout = () => {
        dispatch(attemptLogout())
    }
    return(
        <View style={styles.container}>
            <Text style={{textAlign: 'center', color: 'lightgray'}}>
                Mi Cuenta 
            </Text>
            <TouchableOpacity
                style={styles.buttonStyle}
                onPress={Logout}
            >
                <Icon 
                    type="ionicon"
                    name="log-out-outline"
                    color="#7F00FF"
                />
                <Text style={{textAlign: 'center', color: '#7F00FF', fontSize: 20}}>
                    Cerrar Sesión
                </Text>
            </TouchableOpacity>
        </View>
    )
} 

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems:'center',
        backgroundColor: '#0e0e10',
        justifyContent: 'space-between'
    },
    buttonStyle: {
        justifyContent: 'space-around',
        borderTopWidth: 1,
        borderTopColor: 'lightgray',
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 40,
        paddingLeft: 40,
        height: 60,
        width: '90%',
    },
}) 