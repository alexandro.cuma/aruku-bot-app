import React from 'react';
import { VariablesForm } from './form';
import { useDispatch } from 'react-redux';
import { Validations } from './validations';
import { Formik, FormikProps } from 'formik';
import { attemptVariable } from '../../store/thunks/variables';


const initialValues = {
    name: '',
    type: 'number',
    initialValue: null,

}

interface VariablesFormFields {
    name: string
    type: string
    initialValue: any,
}

export const VariablesModal = () => {
    const dispatch = useDispatch()
    const onSubmit = (values:any) => {
        dispatch(attemptVariable({ name: values.name, type: values.type, initialValue:values.initialValue}))
    }
    return(
        <Formik
            initialValues={initialValues} on
            // validationSchema={Validations}
            onSubmit={onSubmit} 
        >
            {(formikProps: FormikProps<VariablesFormFields>) => <VariablesForm {...formikProps} />}
        </Formik>
    )
} 