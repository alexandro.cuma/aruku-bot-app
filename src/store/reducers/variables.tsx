import { actionNames } from '../actions';

interface Variable {
    name: string
    type: string
    initialValue: any,
}

interface variableState {
    loading: boolean,
    variable: Variable
}

const initialState: variableState = {
    loading: false,
    variable: undefined
}

export const variableReducer = (state: variableState = initialState, action:any) => {
    switch(action.type) {
        case actionNames.VARIABLE_CREATION_ATTEMPT: {
            return {
                ...state,
                loading: true
            }
        }
        case actionNames.VARIABLE_CREATION_COMPLETED: {
            return {
                ...state,
                loading: false
            }
        }
        case actionNames.VARIABLE_CREATION_SUCCESS: {
            if(action.payload.variable){
                return {
                    ...state,
                    loading: false,
                    variable: action.payload.variable
                }
            }
            return state
        }
        default:
            return state
    }
}