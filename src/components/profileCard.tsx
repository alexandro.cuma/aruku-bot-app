import React from 'react'
import { Text, Avatar } from 'react-native-elements'
import { View, TouchableOpacity, StyleSheet, Dimensions, ImageBackground } from 'react-native'

const { width, height } = Dimensions.get('window')

export const ProfileCard = ({ onPress, Info }: any) => {
    return(
        <ImageBackground
            source={{ uri: Info.offline_image_url }}
            style={styles.cardStyle}>
            <View style={styles.userData}>
                <View style={{backgroundColor:'rgba(0,0,0,0.5)', width:width*0.7, borderRadius: 10, marginLeft: 10}}>
                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center'}}>{Info.display_name}</Text>
                    <Text style={{color: 'white', fontSize: 10, textAlign: 'center'}}>{Info.email}</Text>
                </View>
                <TouchableOpacity onPress={onPress}>
                    <Avatar rounded
                        size={60}
                        containerStyle={{margin: 10}}
                        source={{ uri:Info.profile_image_url }}
                    />
                </TouchableOpacity>
            </View>
        </ImageBackground>
    )
}

const styles =  StyleSheet.create({
    cardStyle: {
        width: width,
        height: height*.25,
        backgroundColor: 'transparent',
    }, 
    userData: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: height*.18
    },
})