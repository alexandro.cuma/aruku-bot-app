import React, { useState } from 'react';
import { Picker } from '@react-native-community/picker';
import { View, StyleSheet, Dimensions } from 'react-native';
import { Button, Input, Text, Icon } from 'react-native-elements';

const width = Dimensions.get("window").width

export const VariablesForm = (props: any) => {
    return(
        <View>
            <Text style={styles.textStyle} h3>
                Crea una variable personalizada para tus comandos o timers 
            </Text>
            <Input 
                maxLength={15}
                value={props.values.name}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholder="Nombre variable" 
                errorMessage={props.errors.name}
                onChangeText={props.handleChange('name')}
            />
            <Text style={[styles.textStyle, styles.subtitle]} >Elige el tipo de variable</Text>
            <Picker
                selectedValue={props.values.type}
                style={styles.pickerStyle}
                itemStyle={styles.pickerStyle}
                onValueChange={(value) => props.setFieldValue("type",value)}
            >
                <Picker.Item label="Número" value="number" />
                <Picker.Item label="Texto" value="text" />
            </Picker>
            {
                props.values.type === "number"?
                (
                    <Input 
                    maxLength={2}
                    keyboardType='phone-pad'
                    value={props.values.initialValue}
                    inputStyle={styles.inputStyle}
                    labelStyle={styles.labelStyle}
                    placeholder="Valor de variable" 
                    errorMessage={props.errors.initialNumbValue}
                    onChangeText={props.handleChange('initialValue')}
                />
                ):(
                    <Input 
                    maxLength={20}
                    value={props.values.initialValue}
                    inputStyle={styles.inputStyle}
                    labelStyle={styles.labelStyle}
                    placeholder="Valor de variable" 
                    errorMessage={props.errors.initialTextValue}
                    onChangeText={props.handleChange('initialValue')}
                />
                )      
            }
            <Button buttonStyle={styles.buttonCommand} onPress={props.handleSubmit} title="Crear variable"/>
        </View>
    )
}

const styles = StyleSheet.create({
    inputContainer: {
        backgroundColor: 'white',
        marginHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 10
    },
    labelStyle: {
        color: 'white'
    },
    cooldownValue: {
        textAlign: 'center',
        color: 'lightgray',
        marginBottom: 25,
        marginTop: 25,
        fontSize: 18
    }, 
    buttonCommand: {
        backgroundColor: '#7F00FF',
        color: 'lightgray',        
    },
    textStyle: {
        marginBottom: 50,
        textAlign: 'left',
        color: 'lightgray',
    },
    subtitle: {
        fontSize: 16,
        marginBottom: 0,
        textAlign: 'center'
    },
    inputStyle: {
        color:'white', 
        fontSize: 20
    }, 
    pickerStyle: {
        textAlign: 'center',
        borderRadius: 20,
        color: 'white',
        fontSize: 40
    }, 
})