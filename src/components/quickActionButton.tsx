import React from 'react';
import { Icon, Text } from 'react-native-elements';
import { Dimensions, TouchableOpacity, StyleSheet } from 'react-native'

const { width } = Dimensions.get('window')
export const FastAction = ({ navigation, action, icon }:any) => {
    const doAction = () => {
        if(navigation){
            navigation.openDrawer()
        }
    }
    return(
        <TouchableOpacity
            style={styles.buttonStyle}
            onPress={doAction}
        >
            <Icon
                name={icon}
                size={30}
                color="white"
            />
            <Text style={{textAlign: 'center', color: 'white'}}>
                {action}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    buttonStyle: {
        backgroundColor: '#9146FF',
        justifyContent:'center',
        borderRadius: 5,
        width: width*0.4,
        height: 60,
    }
})