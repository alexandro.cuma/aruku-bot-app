
import * as React from 'react';
import { useSelector } from 'react-redux';
import { createStackNavigator } from '@react-navigation/stack';

import { StreamLogin, Loading } from '../../screens';

const Stack = createStackNavigator();

export const LogStackNavigation = () => {
  const loading = useSelector(state => state.login.loading)
  return (
      <Stack.Navigator>
        {
          loading?(
              <Stack.Screen options={{headerShown: false}} name="Loading" component={Loading} />
          ):(
              <Stack.Screen options={{headerShown: false}} name="Login" component={StreamLogin} />
          )
        }
      </Stack.Navigator>
  );
}
