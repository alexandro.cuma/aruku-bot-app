import React from 'react';
import { Dimensions } from 'react-native'
import { WebView } from 'react-native-webview';

const { width } = Dimensions.get('window')
export const TwitchVideo = ({ username }: any) => {
    console.log(username)
    return(
        <WebView
            source={{ uri: "https://player.twitch.tv/?channel="+username+"&parent=streamernews.example.com&muted=true" }}
            style={{ width: width, backgroundColor: '#0e0e10' }}
            showsHorizontalScrollIndicator={true}
            allowsFullscreenVideo={true}
        />
    )
}