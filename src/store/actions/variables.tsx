import { Action, AnyAction } from 'redux';
import { actionNames } from './index';

export const onVariableAttempt: Action = {
    type: actionNames.VARIABLE_CREATION_ATTEMPT,
}

export const onVariableCompleted: Action = {
    type: actionNames.VARIABLE_CREATION_COMPLETED,
}

export const onVariableSuccess = ({name, type, initialValue}: any): AnyAction => ({
    payload: { variable: { name, type, initialValue }},
    type: actionNames.VARIABLE_CREATION_SUCCESS,
})
