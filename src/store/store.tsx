import AsyncStorage from '@react-native-community/async-storage';
import { persistStore, persistReducer } from 'redux-persist';
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

const persistConfig = {
    // Root
    key: 'root',
    // Storage Method (React Native)
    storage: AsyncStorage,
    // Whitelist (Save Specific Reducers)
    whitelist: [
        'login',
    ],
    // Blacklist (Don't Save Specific Reducers)
    blacklist: [
    ],
}
var aStore:any
const middleware = applyMiddleware(thunk, createLogger())

export const createGlobalStore = (rootReducer:any) => {
    const persistedReducer = persistReducer(persistConfig, rootReducer)
    aStore = createStore(persistedReducer, middleware)
}

export const persistorStore = () => {
    let persistor = persistStore(aStore);
    return persistor
}

export const globalStore = () => {
    return aStore
}
