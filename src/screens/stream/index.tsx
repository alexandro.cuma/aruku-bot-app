import React from 'react'
import { View, StyleSheet, Dimensions } from 'react-native';

import { TwitchVideo, FastAction } from '../../components'

const { width, height } = Dimensions.get('window')

export const Stream = ({ username,  navigation }:any) => {
    return(
        <View style={styles.container}>
            <TwitchVideo username={username} />
            <View style={{height: height*0.4, justifyContent:'space-around'}}>
                <View style={styles.easyLinks}>
                    <FastAction action="Abrir chat" icon="chat" navigation={navigation}/>
                    <FastAction action="30's comercial" icon="tv" navigation={null}/>
                </View>
                <View style={styles.easyLinks}>
                    <FastAction action="Editar info" icon="edit" navigation={null}/>
                    <FastAction action="Crear marca" icon="flag" navigation={null}/>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: height,
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor: '#0e0e10'
    },
    easyLinks: {
        justifyContent:'space-around',
        flexDirection: 'row',
        alignItems:'center',
        width: width,
    },

}) 