import { onInfoAttempt, onInfoCompleted, onInfoSuccess } from '../actions/info';
import { getUserInfo } from '../../services';

export const attemptInfo = ({ token, user }: any) => dispatch => {
    dispatch(onInfoAttempt)
    getUserInfo(token, user)
    .then((data:any) => {
        dispatch(onInfoCompleted)
        dispatch(onInfoSuccess({
            broadcaster_type: data.broadcaster_type,
            picture: data.profile_image_url,
            description: data.description,
            username: data.display_name,
            email: data.email,
        })) 
    })
    .catch((err) => {
        console.log(err)
        dispatch(onInfoCompleted)
    })
}

