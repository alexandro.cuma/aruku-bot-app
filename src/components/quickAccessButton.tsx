import React from 'react'
import { Icon, Text } from 'react-native-elements'
import { View, TouchableOpacity } from 'react-native'

export const QuickAccessButton = ({ navigation, value }: any) => {
    return(
        <View style={{marginLeft: 10, marginRight: 10}}>
            <TouchableOpacity onPress={() => navigation.navigate(value.name)}>
                <Icon
                    raised
                    size={30}
                    name={value.icon}
                    color='#9146FF'
                    reverse={true}
                    type={value.type}
                />
            </TouchableOpacity>
            <Text style={{textAlign: 'center', color: 'lightgray'}}>{value.name}</Text>
        </View>
    )
}