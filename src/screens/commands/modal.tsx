import React from 'react';
import { CommandsForm } from './form';
import { useDispatch } from 'react-redux';
import { Validations } from './validations';
import { Formik, FormikProps } from 'formik';
import { attemptCommand } from '../../store/thunks/commands';


const initialValues = {
    action: '',
    command: '',
    cooldown: 5
}

interface CommandsFormFields {
    action: string
    command: string
    cooldown: number
}

export const CommandsModal = () => {
    const dispatch = useDispatch()
    const onSubmit = (values:any) => {
        dispatch(attemptCommand({command: values.command, action: values.action, cooldown: values.cooldown}))
    }
    return(
        <Formik
            initialValues={initialValues} on
            validationSchema={Validations}
            onSubmit={onSubmit} 
        >
            {(formikProps: FormikProps<CommandsFormFields>) => <CommandsForm {...formikProps} />}
        </Formik>
    )
} 