import { Action, AnyAction } from 'redux';
import { actionNames } from './index';

export const onCommandAttempt: Action = {
    type: actionNames.COMMAND_CREATION_ATTEMPT,
}

export const onCommandCompleted: Action = {
    type: actionNames.COMMAND_CREATION_COMPLETED,
}

export const onCommandSuccess = ({action, command, cooldown}: any): AnyAction => ({
    payload: { command: { action, command, cooldown }},
    type: actionNames.COMMAND_CREATION_SUCCESS,
})
