import { Action, AnyAction } from 'redux';
import { actionNames } from './index';

export const onLoginAttempt: Action = {
    type: actionNames.ARUKUBOT_LOGIN_ATTEMPT,
}

export const onLoginCompleted: Action = {
    type: actionNames.ARUKUBOT_LOGIN_COMPLETED,
}

export const onLoginSuccess = (user: any): AnyAction => ({
    payload: { user },
    type: actionNames.ARUKUBOT_LOGIN_SUCCESS,
})

export const onLogoutAttempt: Action = {
    type: actionNames.ARUKUBOT_USER_LOGOUT
}