import { authorize } from 'react-native-app-auth';
import axios from 'axios';

const url = 'https://id.twitch.tv/oauth2'
const redirectUrl = 'com.aruku.app://redirect'
const clientId = '7j3zgcd85tmodozj7b2xjnx193e8gy'
const clientSecret = 'eb3lw5b0s9stugi7gg1s2hvnn67civ'
const config = {
    skipCodeExchange: true,
    warmAndPrefetchChrome: true,
    issuer: url,
    clientId: clientId,
    redirectUrl: redirectUrl,
    scopes: [
        'user:edit',
        'user:read:email',
        'chat:read',
        'chat:edit'
    ],
    addditionalParameters: {
        claims: '{"id_token":{"email":null,"email_verified":null},"userinfo":{"picture":null}}'
    }
};

export const TwitchLogin = async () => {
    let authCode = await getAuthCode()
    if(authCode){
        let tokens = await getAccessToken(authCode)
        console.log(tokens)
        return tokens
    }
}

const getAuthCode = async () => {
    try {
        let result = await authorize(config)
        return result.authorizationCode
    } catch (err) {
        console.log(err);
    }
}

const getAccessToken = async (code: string) => {
    try {
        let resp = await axios.post(url+'/token?client_id='+clientId+'&client_secret='+clientSecret+'&code='+code+'&grant_type=authorization_code&redirect_uri='+redirectUrl)
        return resp.data
    } catch (err) {
        console.log(err)
    }
}