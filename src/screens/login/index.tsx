import React from 'react';
import { View, StyleSheet } from 'react-native';
import { useDispatch } from 'react-redux';
import { attemptLogin } from '../../store/thunks';
import { SocialIcon } from 'react-native-elements';

export const StreamLogin = () => {
    const dispatch =  useDispatch()
    const onLogin = () => {
        dispatch(attemptLogin())
    }
    return(
        <View style={styles.container}>
            <SocialIcon 
                button
                type="twitch"
                onPress={onLogin}
                title="Iniciar Sesión"
            />
        </View>
    )   
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    }
})