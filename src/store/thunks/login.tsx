import { onLoginAttempt, onLoginCompleted, onLoginSuccess, onLogoutAttempt } from '../actions/login';
import { CommonActions } from '@react-navigation/native';
import { TwitchLogin } from '../../services';

export const attemptLogin = () => dispatch => {
    dispatch(onLoginAttempt)
    TwitchLogin()
    .then((tokens:any) => {
        dispatch(onLoginSuccess({
            access_token: tokens.access_token, 
            refresh_token: tokens.refresh_token
        })) 
        dispatch(onLoginCompleted)
    })
    .catch(() => {
        dispatch(onLoginCompleted)
    })
}

export const attemptLogout = () => dispatch => {
    dispatch(onLogoutAttempt)
    CommonActions.navigate('Login')
}