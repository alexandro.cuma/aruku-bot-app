import { CommandsModal } from './modal';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Loading } from '../loading';
import { CommandList } from './command-list';
import { Icon, Overlay } from 'react-native-elements';
import { View, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';

const width = Dimensions.get("window").width
const height =  Dimensions.get("window").height

export const Commands = () => {
    const loading = useSelector(state => state.command.loading)
    const [visible, setVisible] = useState(false)
    const toggleOverlay = () => {
      setVisible(!visible);
    }
    console.log(loading)
    return(
        <View style={styles.container}>
            <Overlay 
                overlayStyle={styles.overlayStyle} 
                onBackdropPress={toggleOverlay}
                isVisible={visible}
            >   
                <View style={styles.overlayViewStyle}>
                    <TouchableOpacity
                        onPress={() => setVisible(false)}
                        style={styles.closeButtonStyle}
                    >
                        <Icon
                            name="close"
                            size={30}
                            color="black"
                        />
                    </TouchableOpacity>
                    {loading? 
                        (
                            <Loading />
                        ):(
                            <CommandsModal />
                        )
                    }
                </View>
            </Overlay>
            <TouchableOpacity
                style={styles.buttonStyle}
                onPress={toggleOverlay}
            >
                <Icon
                    name="add"
                    size={30}
                    color="white"
                />
            </TouchableOpacity>
            <CommandList />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    closeButtonStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.5,
        shadowRadius: 3.84,
        elevation: 5,
        backgroundColor: 'white',
        justifyContent:'center',
        borderColor: 'white',
        position: 'absolute',
        borderRadius: 50,
        borderWidth: 1,
        height: 40,
        right: -20,
        width: 40,
        top: -20,
    },
    buttonStyle: {
        backgroundColor: '#7F00FF',
        justifyContent:'center',
        position: 'absolute',
        alignItems:'center',
        borderRadius: 50,
        bottom: 10,
        height: 60,
        zIndex: 3,
        right: 10,
        width: 60,
    },
    overlayStyle: {
        backgroundColor: '#1f1f23',
        justifyContent: 'center',
        height: height*.9,
        width: width*.9,
        borderRadius: 10,
    },
    overlayViewStyle: {
        justifyContent: 'center',
        height: '100%',
        width: '100%',
    }
})