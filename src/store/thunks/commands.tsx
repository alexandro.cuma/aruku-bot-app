import { onCommandAttempt, onCommandSuccess, onCommandCompleted } from '../actions/commands';
import { createCommands } from '../../services'
export const attemptCommand = ({command, action, cooldown}: any) => (dispatch:any) => {
    dispatch(onCommandAttempt)
    createCommands({command,action,cooldown})
    .then(() =>{
        dispatch(onCommandCompleted)
        dispatch(onCommandSuccess({command,action,cooldown})) 
    })
    .catch(() =>{
        dispatch(onCommandCompleted)
    })
}
