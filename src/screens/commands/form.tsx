import React from 'react';
import { View, StyleSheet, } from 'react-native';
import { Button, Slider, Input, Text, Icon } from 'react-native-elements';

export const CommandsForm = (props: any) => {
    return(
        <View>
            <Text
                style={styles.textStyle}
                h3
            >Crea un comando personalizado para tu stream</Text>
            <Input 
                maxLength={10}
                value={props.values.command}
                inputStyle={styles.inputStyle}
                labelStyle={styles.labelStyle}
                placeholder="Escribe el comando..." 
                errorMessage={props.errors.command}
                onChangeText={props.handleChange('command')}
                leftIcon={
                    <Icon 
                        size={18}
                        name="priority-high"
                        color="lightgray"
                    />
                }
            />
            <Input 
                maxLength={180}
                multiline={true}
                inputStyle={styles.inputStyle}
                placeholder="Escribe la accion..."
                errorMessage={props.errors.action}
                onChangeText={props.handleChange('action')}
            />
            <Slider 
                step={0.5}
                minimumValue={2}
                maximumValue={10}
                thumbTintColor="#7F00FF"
                value={props.values.cooldown} 
                maximumTrackTintColor="gray"
                minimumTrackTintColor="#7F00FF"
                onValueChange={(value)=>props.setFieldValue('cooldown',value)}
            />
            <Text style={styles.cooldownValue}>Tiempo de Reutilización Global : {"\n"} {props.values.cooldown} segundos</Text>
            <Button buttonStyle={styles.buttonCommand} onPress={props.handleSubmit} title="Crear comando"/>
        </View>
    )
}

const styles = StyleSheet.create({
    inputContainer: {
        backgroundColor: 'white',
        marginHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 10
    },
    labelStyle: {
        color: 'white'
    },
    cooldownValue: {
        textAlign: 'center',
        color: 'lightgray',
        marginBottom: 25,
        marginTop: 25,
        fontSize: 18
    }, 
    buttonCommand: {
        backgroundColor: '#7F00FF',
        color: 'lightgray'
        
    },
    textStyle: {
        marginBottom: 50,
        textAlign: 'left',
        color: 'lightgray',
    },
    inputStyle: {
        color:'white', 
        fontSize: 20
    }
})