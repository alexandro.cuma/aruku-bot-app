import { actionNames } from '../actions';

interface Command {
    command: string
    action: string
    cooldown: any,
}

interface CommandState {
    loading: boolean,
    command: Command
}

const initialState: CommandState = {
    loading: false,
    command: undefined
}

export const commandReducer = (state: CommandState = initialState, action:any) => {
    switch(action.type) {
        case actionNames.COMMAND_CREATION_ATTEMPT: {
            return {
                ...state,
                loading: true
            }
        }
        case actionNames.COMMAND_CREATION_COMPLETED: {
            return {
                ...state,
                loading: false
            }
        }
        case actionNames.COMMAND_CREATION_SUCCESS: {
            if(action.payload.command){
                return {
                    ...state,
                    loading: false,
                    command: action.payload.command
                }
            }
            return state
        }
        default:
            return state
    }
}