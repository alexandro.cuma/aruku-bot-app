import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { Stream } from '../../screens'
import { TwitchChat } from '../../components';

const Drawer = createDrawerNavigator();

export const StreamDrawNavigation = ({ route }: any) => {
  const { username } = route.params
  const passDraw = ({ navigation }) => {
    return(
      <TwitchChat username={username} navigation={navigation}/>
    )
  }
  const passComponent = ({ navigation }) =>{
    return(
      <Stream username={username} navigation={navigation}/>
    )
  }
  return (
    <Drawer.Navigator
      lazy={false}
      drawerType="slide"
      drawerContent={passDraw}  
      initialRouteName="StreamVideo"
      >
        <Drawer.Screen name="StreamVideo" component={passComponent}/>
    </Drawer.Navigator>
  );
}

