import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import { ListItem, Text } from 'react-native-elements';

const variableList:any = []

export const VariableList = () => {
    const variable = useSelector(state => state.variable.variable)
    
    useEffect(()=>{
        if(variable){
            let index = variableList.findIndex(x => x.name === variable.name)
            if(index < 0){
                variableList.push(variable)
            }
        }
    })
    return(
        <View style={styles.container}>
            <Text style={styles.textStyle} h3>Variables de Directo</Text>
            {
                variableList.map((l, i) => (
                <ListItem containerStyle={styles.listItem} key={i} bottomDivider>
                    {/* <Avatar source={{uri: l.avatar_url}} /> */}
                    <ListItem.Content>
                    <ListItem.Title style={styles.listTextStyle}>
                        {l.name}
                    </ListItem.Title>
                    <ListItem.Subtitle style={styles.listSubTextStyle}>
                        {l.initialValue}
                    </ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
                ))
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0e0e10'
    },
    textStyle: {
        textAlign: 'center',
        marginBottom: 20,
        marginTop: 20,
        color:'lightgray'
    },
    listItem: {
        backgroundColor: '#18181b',
    },
    listTextStyle: {
        fontSize: 20,
        color: 'lightgray'
    },
    listSubTextStyle: {
        fontSize: 16,
        color: 'lightgray'
    }
})